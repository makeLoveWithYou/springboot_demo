package com.export.springbootexport.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 评价表
 * @date 2018/8/22 11:26
 */
@Data
@Entity(name = "t_evaluate")
public class Evaluate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    private Integer cid;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Column(name = "create_time")
    private Timestamp createTime;

    private String content;

    @Transient
    private String[] title;

    @Transient
    private Integer[] orders;

    @Transient
    private Date time1;

    @Transient
    private Date time2;


    public Evaluate() {

    }

    public Evaluate(int id, int userId, String content, Timestamp time) {
        this.id = id;
        this.content = content;
        this.createTime = time;
        this.userId = userId;
    }

}
