package com.springbootlayuiangular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLayuiAngularApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLayuiAngularApplication.class, args);
    }
}
